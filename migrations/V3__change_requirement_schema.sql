ALTER TABLE requirements
    RENAME TO version_requirements;

CREATE TABLE requirements (
    spec text PRIMARY KEY
);

CREATE TABLE requirement_descriptions (
    spec text NOT NULL
        REFERENCES requirements (spec)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    lang text NOT NULL,
    description text NOT NULL,
    PRIMARY KEY (spec, lang)
);

ALTER TABLE version_requirements
    DROP COLUMN id,
    ADD PRIMARY KEY (package, version, spec),
    ADD FOREIGN KEY (spec)
        REFERENCES requirements (spec)
        ON DELETE CASCADE
        ON UPDATE CASCADE;
