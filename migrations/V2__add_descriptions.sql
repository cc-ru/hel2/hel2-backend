CREATE TABLE descriptions (
    package text NOT NULL
        REFERENCES packages (name)
            ON DELETE CASCADE,
    lang text NOT NULL,
    description text NOT NULL,
    PRIMARY KEY (package, lang)
);