CREATE TABLE packages (
    name text PRIMARY KEY,
    license text NOT NULL,
    latest_version text NULL,
    created_date timestamp NOT NULL,
    created_user text NOT NULL,
    last_updated_date timestamp NOT NULL,
    last_updated_user text NOT NULL
);

CREATE TABLE users (
    username text PRIMARY KEY,
    email text NOT NULL,
    status text NOT NULL,
    avatar text NULL,
    registered_date timestamp NOT NULL,
    role text NOT NULL,
    password_hash bytea NOT NULL,
    salt bytea NOT NULL
);

CREATE TABLE summaries (
    package text NOT NULL
        REFERENCES packages (name)
            ON DELETE CASCADE,
    lang text NOT NULL,
    summary text NOT NULL,
    PRIMARY KEY (package, lang)
);

CREATE TABLE authors (
    id serial PRIMARY KEY,
    package text NOT NULL
        REFERENCES packages (name)
            ON DELETE CASCADE,
    author text NOT NULL
);

CREATE TABLE maintainers (
    package text NOT NULL
        REFERENCES packages (name)
            ON DELETE CASCADE,
    username text NOT NULL
        REFERENCES users (username)
            ON DELETE CASCADE,
    PRIMARY KEY (package, username)
);

CREATE TABLE stargazers (
    package text NOT NULL
        REFERENCES packages (name)
            ON DELETE CASCADE,
    username text NOT NULL
        REFERENCES users (username)
            ON DELETE CASCADE,
    PRIMARY KEY (package, username)
);

CREATE TABLE versions (
    package text NOT NULL
        REFERENCES packages (name)
            ON DELETE CASCADE,
    version text NOT NULL,
    download_count integer NOT NULL,
    PRIMARY KEY (package, version)
);

ALTER TABLE packages
    ADD FOREIGN KEY (name, latest_version)
        REFERENCES versions (package, version)
        ON DELETE SET NULL;

CREATE TABLE dependencies (
    package text NOT NULL,
    version text NOT NULL,
    dependency text NOT NULL
        REFERENCES packages (name)
            ON DELETE RESTRICT,
    spec text NOT NULL,
    PRIMARY KEY (package, version, dependency),
    FOREIGN KEY (package, version)
        REFERENCES versions (package, version)
        ON DELETE CASCADE
);

CREATE TABLE changelogs (
    package text NOT NULL,
    version text NOT NULL,
    lang text NOT NULL,
    changelog text NOT NULL,
    PRIMARY KEY (package, version, lang),
    FOREIGN KEY (package, version)
        REFERENCES versions (package, version)
        ON DELETE CASCADE
);

CREATE TABLE requirements (
    id serial PRIMARY KEY,
    package text NOT NULL,
    version text NOT NULL,
    spec text NOT NULL,
    FOREIGN KEY (package, version)
        REFERENCES versions (package, version)
        ON DELETE CASCADE
);

CREATE TABLE tags (
    id serial PRIMARY KEY
);

CREATE TABLE package_tags (
    package text NOT NULL
        REFERENCES packages (name)
        ON DELETE CASCADE,
    tag integer NOT NULL
        REFERENCES tags (id)
        ON DELETE CASCADE,
    PRIMARY KEY (package, tag)
);

CREATE TABLE tag_names (
    tag integer NOT NULL
        REFERENCES tags (id)
        ON DELETE CASCADE,
    lang text NOT NULL,
    name text NOT NULL,
    PRIMARY KEY (tag, lang)
);