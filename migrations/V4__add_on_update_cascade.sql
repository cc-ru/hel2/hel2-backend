ALTER TABLE summaries
    DROP CONSTRAINT summaries_package_fkey,
    ADD FOREIGN KEY (package)
        REFERENCES packages (name)
        ON DELETE CASCADE
        ON UPDATE CASCADE;

ALTER TABLE authors
    DROP CONSTRAINT authors_package_fkey,
    ADD FOREIGN KEY (package)
        REFERENCES packages (name)
        ON DELETE CASCADE
        ON UPDATE CASCADE;

ALTER TABLE maintainers
    DROP CONSTRAINT maintainers_package_fkey,
    DROP CONSTRAINT maintainers_username_fkey,
    ADD FOREIGN KEY (package)
        REFERENCES packages (name)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    ADD FOREIGN KEY (username)
        REFERENCES users (username)
        ON DELETE CASCADE
        ON UPDATE CASCADE;

ALTER TABLE stargazers
    DROP CONSTRAINT stargazers_package_fkey,
    DROP CONSTRAINT stargazers_username_fkey,
    ADD FOREIGN KEY (package)
        REFERENCES packages (name)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    ADD FOREIGN KEY (username)
        REFERENCES users (username)
        ON DELETE CASCADE
        ON UPDATE CASCADE;

ALTER TABLE versions
    DROP CONSTRAINT versions_package_fkey,
    ADD FOREIGN KEY (package)
        REFERENCES packages (name)
        ON DELETE CASCADE
        ON UPDATE CASCADE;

ALTER TABLE packages
    DROP CONSTRAINT packages_name_latest_version_fkey,
    ADD FOREIGN KEY (name, latest_version)
        REFERENCES versions (package, version)
        ON DELETE SET NULL
        ON UPDATE CASCADE;

ALTER TABLE dependencies
    DROP CONSTRAINT dependencies_dependency_fkey,
    DROP CONSTRAINT dependencies_package_version_fkey,
    ADD FOREIGN KEY (dependency)
        REFERENCES packages (name)
        ON DELETE RESTRICT
        ON UPDATE CASCADE,
    ADD FOREIGN KEY (package, version)
        REFERENCES versions (package, version)
        ON DELETE CASCADE
        ON UPDATE CASCADE;

ALTER TABLE changelogs
    DROP CONSTRAINT changelogs_package_version_fkey,
    ADD FOREIGN KEY (package, version)
        REFERENCES versions (package, version)
        ON DELETE CASCADE
        ON UPDATE CASCADE;

ALTER TABLE package_tags
    DROP CONSTRAINT package_tags_package_fkey,
    DROP CONSTRAINT package_tags_tag_fkey,
    ADD FOREIGN KEY (package)
        REFERENCES packages (name)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    ADD FOREIGN KEY (tag)
        REFERENCES tags (id)
        ON DELETE CASCADE
        ON UPDATE CASCADE;

ALTER TABLE tag_names
    DROP CONSTRAINT tag_names_tag_fkey,
    ADD FOREIGN KEY (tag)
        REFERENCES tags (id)
        ON DELETE CASCADE
        ON UPDATE CASCADE;

ALTER TABLE descriptions
    DROP CONSTRAINT descriptions_package_fkey,
    ADD FOREIGN KEY (package)
        REFERENCES packages (name)
            ON DELETE CASCADE
            ON UPDATE CASCADE;
