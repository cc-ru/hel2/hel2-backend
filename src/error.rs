use actix_web::http::StatusCode;
use actix_web::web::HttpResponse;
use thiserror::Error;

use crate::api::models::ApiResponse;
use crate::api::validation::ValidationError;

pub type SqlError = sqlx::Error<sqlx::Postgres>;

#[derive(Debug, Error)]
pub enum ApiError {
    #[error("Internal error")]
    SqlError(#[from] SqlError),
    #[error("Internal error")]
    ModelError(#[source] anyhow::Error),
    #[error("Not found")]
    NotFound,
    #[error("Package already exists: {0}")]
    PackageExists(String),
    #[error("{0}")]
    ValidationError(#[from] ValidationError),
    #[error("Invalid username and/or password")]
    InvalidCredentials,

    #[error("Internal error")]
    InternalError(#[source] Option<anyhow::Error>),
}

impl actix_web::ResponseError for ApiError {
    fn status_code(&self) -> StatusCode {
        use ApiError::*;

        match self {
            SqlError(_) | ModelError(_) | InternalError(_) => StatusCode::INTERNAL_SERVER_ERROR,
            InvalidCredentials => StatusCode::UNAUTHORIZED,
            NotFound => StatusCode::NOT_FOUND,
            PackageExists(_) => StatusCode::CONFLICT,
            ValidationError(_) => StatusCode::BAD_REQUEST,
        }
    }

    fn error_response(&self) -> HttpResponse {
        HttpResponse::build(self.status_code()).json(ApiResponse::Error::<()> {
            message: self.to_string(),
        })
    }
}
