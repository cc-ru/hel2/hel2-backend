use std::collections::HashSet;

use serde::{Deserialize, Serialize};
use time::OffsetDateTime;

use crate::State;

use super::validation::{
    validate_dependencies, validate_length, validate_requirements, validate_tags, validate_users,
    validate_version, validate_version_spec, Validate, ValidationErrorKind, ValidationResult,
    ValidationResultExt,
};
use super::Language;

#[derive(Serialize, Deserialize)]
pub struct LocalizedText {
    pub lang: Language,
    pub text: String,
}

impl super::GetLanguage for LocalizedText {
    fn get_language(&self) -> &Language {
        &self.lang
    }
}

impl Default for LocalizedText {
    fn default() -> Self {
        Self {
            lang: Language::Lang("en-US".to_owned()),
            text: "".to_owned(),
        }
    }
}

#[async_trait]
impl Validate for Vec<LocalizedText> {
    type Output = ();

    async fn validate(&self, _state: &State) -> ValidationResult<Self::Output> {
        if self.is_empty() {
            return Err(ValidationErrorKind::NoDataSpecified.field(field!()));
        }

        let mut languages = HashSet::new();

        for (i, LocalizedText { ref lang, .. }) in self.iter().enumerate() {
            if languages.contains(lang) {
                Err(ValidationErrorKind::DuplicateValue(lang.to_string()).field(i.to_string()))?;
            }

            languages.insert(lang);
        }

        Ok(())
    }
}

#[derive(Serialize)]
#[serde(tag = "type")]
pub enum ApiResponse<T: Serialize> {
    #[serde(rename = "success")]
    Success {
        #[serde(skip_serializing_if = "Option::is_none")]
        data: Option<T>,
    },
    #[serde(rename = "error")]
    Error { message: String },
}

impl<T: Serialize> From<Option<T>> for ApiResponse<T> {
    fn from(data: Option<T>) -> Self {
        Self::Success { data }
    }
}

impl<T: Serialize> From<T> for ApiResponse<T> {
    fn from(data: T) -> Self {
        Self::Success { data: Some(data) }
    }
}

#[derive(Serialize, Deserialize)]
pub struct UserShort {
    pub username: String,
    pub role: String,
}

#[derive(Serialize)]
pub struct Package {
    pub name: String,
    pub summaries: Vec<LocalizedText>,
    pub descriptions: Vec<LocalizedText>,
    pub license: String,
    pub authors: Vec<String>,
    pub maintainers: Vec<UserShort>,
    pub tags: Vec<Tag>,
    pub starred: i64,
    pub history: History,
}

#[derive(Deserialize)]
pub struct NewPackage {
    pub summaries: Vec<LocalizedText>,
    pub descriptions: Vec<LocalizedText>,
    pub license: String,
    pub authors: Vec<String>,
    pub maintainers: Vec<String>,
    pub tags: Vec<i32>,
    pub versions: Vec<Version>,
}

#[async_trait]
impl Validate for NewPackage {
    type Output = ();

    async fn validate(&self, state: &State) -> ValidationResult<Self::Output> {
        // TODO: at least 1 summary, etc.
        self.summaries.validate(state).await.field("summaries")?;
        self.descriptions
            .validate(state)
            .await
            .field("descriptions")?;
        validate_length(&self.license, 0..=200).field("license")?;
        self.authors
            .iter()
            .enumerate()
            .try_for_each(|(i, author)| {
                validate_length(author, 0..=50).field(field!("authors", i.to_string()))
            })?;
        validate_users(&self.maintainers, &state.db)
            .await
            .field("maintainers")?;
        validate_tags(&self.tags, &state.db).await.field("tags")?;
        self.versions.validate(state).await.field("versions")?;

        Ok(())
    }
}

#[derive(Serialize)]
pub struct Tag {
    pub id: i32,
    pub names: Vec<LocalizedText>,
}

#[derive(Serialize, Deserialize)]
pub struct History {
    pub latest_version: String,
    pub created: HistoryAction,
    pub last_updated: HistoryAction,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub versions: Option<Vec<Version>>,
}

#[derive(Serialize, Deserialize)]
pub struct HistoryAction {
    pub date: OffsetDateTime,
    pub actor: UserShort,
}

#[derive(Serialize, Deserialize)]
pub struct Version {
    pub version: String,
    #[serde(skip_deserializing)]
    pub downloads: i32,
    pub requirements: Vec<Requirement>,
    #[serde(skip_deserializing)]
    pub file_index_url: String,
    #[serde(skip_deserializing)]
    pub archive_url: String,
    pub dependencies: Vec<Dependency>,
    pub changelogs: Vec<LocalizedText>,
}

#[async_trait]
impl Validate for Vec<Version> {
    type Output = ();

    async fn validate(&self, state: &State) -> ValidationResult<Self::Output> {
        for (i, version) in self.iter().enumerate() {
            validate_version(&version.version).field(field!(i.to_string(), "version"))?;
            version
                .changelogs
                .validate(state)
                .await
                .field(field!(i.to_string(), "changelogs"))?;
            version
                .dependencies
                .iter()
                .enumerate()
                .try_for_each(|(i, Dependency { spec, .. })| {
                    validate_version_spec(&spec).field(i.to_string())
                })
                .field(field!(i.to_string(), "dependencies"))?;
        }

        let requirements: Vec<_> = self
            .iter()
            .enumerate()
            .flat_map(
                |(
                    i,
                    Version {
                        ref requirements, ..
                    },
                )| {
                    requirements
                        .iter()
                        .enumerate()
                        .map(move |(j, req)| (i, j, &req.spec))
                },
            )
            .collect();

        validate_requirements(requirements, &state.db)
            .await
            .map_err(|(idx, err)| match idx {
                Some((i, j)) => err.field(field!(i.to_string(), "requirements", j.to_string())),
                None => err.field(field!()),
            })?;

        let dependencies: Vec<_> = self
            .iter()
            .enumerate()
            .flat_map(
                |(
                    i,
                    Version {
                        ref dependencies, ..
                    },
                )| {
                    dependencies
                        .iter()
                        .enumerate()
                        .map(move |(j, dep)| (i, j, &dep.package))
                },
            )
            .collect();

        validate_dependencies(dependencies, &state.db)
            .await
            .map_err(|(idx, err)| match idx {
                Some((i, j)) => err.field(field!(i.to_string(), "dependencies", j.to_string())),
                None => err.field(field!()),
            })?;

        Ok(())
    }
}

#[derive(Serialize, Deserialize)]
pub struct Dependency {
    pub package: String,
    pub spec: String,
}

#[derive(Serialize, Deserialize)]
pub struct Requirement {
    pub spec: String,
    #[serde(skip_deserializing)]
    pub descriptions: Vec<LocalizedText>,
}

#[derive(Deserialize)]
pub struct LoginCredentials {
    pub username: String,
    pub password: String,
}

#[derive(Serialize, Deserialize)]
pub struct SessionData {
    pub username: String,
    pub role: String,
}
