macro_rules! zip_by_key {
    ($left:expr, $right:expr, $key:tt) => {
        $crate::api::zip_by($left, $right, |l, r| l.$key.cmp(&r.$key))
            .map($crate::api::macros::flatten)
    };
}

pub fn flatten<K, L, R>((l, r): (Option<(K, L)>, Option<(K, R)>)) -> (K, (Option<L>, Option<R>)) {
    match (l, r) {
        (Some((key, left)), Some((_, right))) => (key, (Some(left), Some(right))),
        (Some((key, left)), None) => (key, (Some(left), None)),
        (None, Some((key, right))) => (key, (None, Some(right))),
        (None, None) => panic!("both values are None"),
    }
}

macro_rules! field {
    ($root:expr, $($subfield:expr),*,) => (field!($root, $($subfield,)*));

    ($root:expr, $($subfield:expr),*) => {
        $crate::api::validation::Field::from($root)$(.append($subfield))*
    };

    ($root:expr,) => (field!($root));

    ($root:expr) => {
        $crate::api::validation::Field::from($root)
    };

    () => {
        $crate::api::validation::Field::new()
    };
}
