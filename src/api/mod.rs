#[macro_use]
mod macros;

pub mod auth;
pub mod models;
pub mod packages;
pub mod validation;

use std::cmp::Ordering;
use std::collections::HashMap;
use std::convert::TryFrom;
use std::fmt::Display;
use std::iter::Peekable;
use std::task::{Context, Poll};

use actix_service::{Service, Transform};
use actix_web::dev::{ServiceRequest, ServiceResponse};
use actix_web::web::{HttpRequest, HttpResponse};
use actix_web::ResponseError;
use futures::future::{ok, LocalBoxFuture, Ready};
use futures::stream::iter;
use futures::{FutureExt, TryStreamExt};
use inflector::cases::sentencecase::to_sentence_case;
use regex::Regex;

use crate::error::ApiError;

type Result<T = HttpResponse, E = ApiError> = std::result::Result<T, E>;

pub struct ZipBy<L, R, K>
where
    L: Iterator,
    R: Iterator,
    K: Fn(&L::Item, &R::Item) -> Ordering,
{
    left: Peekable<L>,
    right: Peekable<R>,
    cmp: K,
}

impl<L, R, K> Iterator for ZipBy<L, R, K>
where
    L: Iterator,
    R: Iterator,
    K: Fn(&L::Item, &R::Item) -> Ordering,
{
    type Item = (Option<L::Item>, Option<R::Item>);

    fn next(&mut self) -> Option<Self::Item> {
        let (left, right) = match (self.left.peek(), self.right.peek()) {
            (None, None) => return None,
            (None, _) => return Some((None, self.right.next())),
            (_, None) => return Some((self.left.next(), None)),
            (Some(l), Some(r)) => (l, r),
        };

        match (self.cmp)(left, right) {
            Ordering::Less => Some((self.left.next(), None)),
            Ordering::Greater => Some((None, self.right.next())),
            Ordering::Equal => Some((self.left.next(), self.right.next())),
        }
    }
}

pub fn zip_by<L, R, K>(left: L, right: R, cmp: K) -> ZipBy<L, R, K>
where
    L: Iterator,
    R: Iterator,
    K: Fn(&L::Item, &R::Item) -> Ordering,
{
    ZipBy {
        left: left.peekable(),
        right: right.peekable(),
        cmp,
    }
}

#[derive(Hash, Clone)]
pub enum Language {
    Lang(String),
    Any,
}

impl Display for Language {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Language::Lang(s) => &s,
                Language::Any => "*",
            }
        )
    }
}

impl serde::Serialize for Language {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_str(&self.to_string())
    }
}

impl TryFrom<String> for Language {
    type Error = validation::ValidationErrorKind;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        lazy_static::lazy_static! {
            static ref LANGUAGE_REGEX: Regex = Regex::new("^[[:alpha:]]{2}(:?\\-[[:alpha:]]{2})$").unwrap();
        }

        if value == "*" {
            Ok(Self::Any)
        } else if LANGUAGE_REGEX.is_match(&value) {
            Ok(Self::Lang(value))
        } else {
            Err(Self::Error::InvalidLanguage(value))
        }
    }
}

struct LanguageVisitor;

impl<'de> serde::de::Visitor<'de> for LanguageVisitor {
    type Value = Language;

    fn expecting(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str("a language code (e. g. en, en-US)")
    }

    fn visit_string<E>(self, value: String) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        Language::try_from(value).map_err(|e| E::custom(e))
    }
}

impl<'de> serde::Deserialize<'de> for Language {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        deserializer.deserialize_string(LanguageVisitor)
    }
}

impl PartialEq for Language {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Language::Lang(left), Language::Lang(right)) => left == right,
            (Language::Any, Language::Any) => true,
            _ => false,
        }
    }
}

impl Eq for Language {}

pub trait GetLanguage {
    fn get_language(&self) -> &Language;
}

impl GetLanguage for Language {
    fn get_language(&self) -> &Language {
        self
    }
}

pub struct Languages(HashMap<Language, usize>);

impl Languages {
    fn choose<'a, T: GetLanguage>(&self, values: impl Iterator<Item = T>) -> Vec<T> {
        // TODO: use the default languages set in the backend config
        values
            .fold(None, |acc, v| {
                if let Some(pos) = self.0.get(&v.get_language()) {
                    if let Some((_, i)) = acc {
                        if pos < i {
                            Some((v, pos))
                        } else {
                            acc
                        }
                    } else {
                        Some((v, pos))
                    }
                } else {
                    acc.or(Some((v, &std::usize::MAX)))
                }
            })
            .map(|(v, _)| vec![v])
            .unwrap_or_else(Vec::new)
    }
}

impl actix_web::FromRequest for Languages {
    type Error = ApiError;
    type Future = Ready<Result<Self, Self::Error>>;
    type Config = Self;

    fn from_request(req: &HttpRequest, _payload: &mut actix_web::dev::Payload) -> Self::Future {
        // TODO: use these sources, ordered by priority:
        //       1. ?lang
        //       2. user profile settings
        //       3. Accept-Language
        let lang = req
            .head()
            .headers
            .get("Accept-Language")
            .and_then(|header| header.to_str().ok());

        if let Some(value) = lang {
            let mut languages = value
                .split(',')
                .filter_map(|group| {
                    let value = group.trim().splitn(1, ';').take(2).collect::<Vec<_>>();
                    let priority = value.get(1).and_then(|v| v.parse().ok()).unwrap_or(1f32);

                    match value[0] {
                        "" => None,
                        "*" => Some((Language::Any, priority)),
                        s => Some((Language::Lang(s.to_owned()), priority)),
                    }
                })
                .collect::<Vec<_>>();

            languages
                .sort_by(|(_, left), (_, right)| left.partial_cmp(right).unwrap_or(Ordering::Less));
            languages.reverse();

            let languages = languages
                .into_iter()
                .enumerate()
                .map(|(pos, (lang, _))| (lang, pos))
                .collect::<HashMap<_, _>>();

            if languages.is_empty() {
                ok(Default::default())
            } else {
                ok(Self(languages))
            }
        } else {
            ok(Default::default())
        }
    }
}

impl Default for Languages {
    fn default() -> Self {
        let mut map = HashMap::with_capacity(1);
        map.insert(Language::Any, 0);

        Self(map)
    }
}

async fn get_service_response_body(
    service_response: &mut ServiceResponse,
) -> Result<String, actix_web::Error> {
    let status = service_response.status();

    let body = service_response
        .take_body()
        .map_ok(|bytes| {
            iter(
                bytes
                    .into_iter()
                    .map(|x| Result::<_, actix_web::Error>::Ok(x)),
            )
        })
        .try_flatten()
        .try_collect::<Vec<_>>()
        .await?;

    let message = String::from_utf8_lossy(&body);

    Ok(if message == "" {
        to_sentence_case(status.canonical_reason().unwrap_or(""))
    } else {
        message.into_owned()
    })
}

pub struct ResponseWrapper;

impl<S> Transform<S> for ResponseWrapper
where
    S: Service<Request = ServiceRequest, Response = ServiceResponse, Error = actix_web::Error>,
    S::Future: 'static,
{
    type Request = ServiceRequest;
    type Response = ServiceResponse;
    type Error = actix_web::Error;
    type InitError = ();
    type Transform = ResponseWrapperMiddleware<S>;
    type Future = Ready<Result<Self::Transform, Self::InitError>>;

    fn new_transform(&self, service: S) -> Self::Future {
        ok(ResponseWrapperMiddleware { service })
    }
}

pub struct ResponseWrapperMiddleware<S> {
    service: S,
}

impl<S> Service for ResponseWrapperMiddleware<S>
where
    S: Service<Request = ServiceRequest, Response = ServiceResponse, Error = actix_web::Error>,
    S::Future: 'static,
{
    type Request = ServiceRequest;
    type Response = ServiceResponse;
    type Error = actix_web::Error;
    type Future = LocalBoxFuture<'static, Result<Self::Response, Self::Error>>;

    fn poll_ready(&mut self, ctx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        self.service.poll_ready(ctx)
    }

    fn call(&mut self, request: ServiceRequest) -> Self::Future {
        let future = self.service.call(request);

        async move {
            let mut service_response = future.await?;

            if let Some(error) = service_response.response().error() {
                if let Some(_) = error.as_error::<ApiError>() {
                    return Ok(service_response);
                }
            }

            let status = service_response.status();

            if status.is_client_error() {
                *service_response.response_mut() = HttpResponse::build_from(std::mem::replace(
                    service_response.response_mut(),
                    HttpResponse::new(status),
                ))
                .json(models::ApiResponse::Error::<()> {
                    message: get_service_response_body(&mut service_response).await?,
                });

                Ok(service_response)
            } else if status.is_server_error() {
                Ok(service_response
                    .map_body(|_, _| ApiError::InternalError(None).error_response().take_body()))
            } else {
                Ok(service_response)
            }
        }
        .boxed_local()
    }
}
