use std::collections::HashMap;
use std::convert::TryFrom;

use actix_web::{web, HttpResponse};
use anyhow::anyhow;
use futures::prelude::*;
use itertools::Itertools;
use time::OffsetDateTime;

use crate::error::ApiError;
use crate::State;

use super::models::{
    ApiResponse, Dependency, History, HistoryAction, LocalizedText, NewPackage, Package,
    Requirement, Tag, UserShort, Version,
};
use super::validation::{Validate, ValidationErrorKind, ValidationResultExt};
use super::{zip_by, Language, Languages, Result};

fn map_to_vec<I>(opt: Option<impl Iterator<Item = I>>) -> Option<Vec<I>> {
    opt.map::<Vec<_>, _>(Iterator::collect)
}

pub fn configure(cfg: &mut web::ServiceConfig) {
    cfg.route("/", web::get().to(list)).service(
        web::resource("/{package}")
            .route(web::get().to(get))
            .route(web::post().to(new_package)),
    );
}

async fn get_package_info(
    name: &String,
    pool: &sqlx::PgPool,
    lang: &Languages,
) -> Result<Package, ApiError> {
    let pkg = sqlx::query!(
        r#"
        SELECT name, license, latest_version,
            created.username AS created_user,created.role AS created_user_role,
            last_updated.username AS last_updated_user, last_updated.role AS last_updated_user_role,
            created_date, last_updated_date
        FROM packages
            LEFT JOIN users AS created
            ON (created.username = created_user)
            LEFT JOIN users AS last_updated
            ON (last_updated.username = last_updated_user)
        WHERE name = $1
        "#,
        *name
    )
    .fetch_one(pool)
    .await
    .map_err(|e| match e {
        sqlx::Error::RowNotFound => ApiError::NotFound,
        _ => e.into(),
    })?;

    let summaries = lang.choose(
        sqlx::query!(
            r#"
                SELECT lang, summary AS "text"
                FROM summaries
                WHERE package = $1
                "#,
            pkg.name
        )
        .fetch(pool)
        .map_err(ApiError::from)
        .and_then(|rec| async move {
            Ok(LocalizedText {
                lang: Language::try_from(rec.lang).map_err(|e| {
                    ApiError::ModelError(e.field(field!(name, "summaries", "lang")).into())
                })?,
                text: rec.text,
            })
        })
        .try_collect::<Vec<_>>()
        .await?
        .into_iter(),
    );

    let descriptions = lang.choose(
        sqlx::query!(
            r#"
                SELECT lang, description AS "text"
                FROM descriptions
                WHERE package = $1
                "#,
            pkg.name
        )
        .fetch(pool)
        .map_err(ApiError::from)
        .and_then(|rec| async move {
            Ok(LocalizedText {
                lang: Language::try_from(rec.lang).map_err(|e| {
                    ApiError::ModelError(e.field(field!(name, "descriptions", "lang")).into())
                })?,
                text: rec.text,
            })
        })
        .try_collect::<Vec<_>>()
        .await?
        .into_iter(),
    );

    let authors = sqlx::query!("SELECT author FROM authors WHERE package = $1", pkg.name)
        .fetch_all(pool)
        .await?;

    let maintainers = sqlx::query_as!(
        UserShort,
        r#"
        SELECT username, role
        FROM maintainers
            INNER JOIN users
            USING (username)
        WHERE maintainers.package = $1
        "#,
        pkg.name
    )
    .fetch_all(pool)
    .await?;

    let tags = sqlx::query!(
        r#"
        SELECT tag, lang, name as "text"
        FROM package_tags
            INNER JOIN tag_names
            USING (tag)
        WHERE package = $1
        "#,
        pkg.name
    )
    .fetch(pool)
    .map_err(ApiError::from)
    .and_then(|rec| async {
        let lang = rec.lang;
        let tag = rec.tag;
        let text = rec.text;

        Ok((
            tag,
            LocalizedText {
                lang: Language::try_from(lang).map_err(|e| {
                    ApiError::ModelError(
                        e.field(field!(name, "tags", tag.to_string(), "lang"))
                            .into(),
                    )
                })?,
                text,
            },
        ))
    })
    .try_collect::<Vec<_>>()
    .await?
    .into_iter()
    .group_by(|v| v.0)
    .into_iter()
    .map(|(tag, texts)| Tag {
        id: tag,
        names: lang.choose(texts.map(|(_, text)| text)),
    })
    .collect::<Vec<_>>();

    let starred = sqlx::query!(
        r#"
        SELECT count(*) AS stars
        FROM stargazers
        WHERE package = $1
        "#,
        pkg.name
    )
    .fetch_one(pool)
    .await?;

    let response = Package {
        name: pkg.name,
        summaries,
        descriptions,
        license: pkg.license,
        authors: authors.into_iter().map(|author| author.author).collect(),
        maintainers,
        tags,
        starred: starred.stars,
        history: History {
            latest_version: pkg.latest_version,
            created: HistoryAction {
                date: pkg.created_date.assume_utc(),
                actor: UserShort {
                    username: pkg.created_user,
                    role: pkg.created_user_role,
                },
            },
            last_updated: HistoryAction {
                date: pkg.last_updated_date.assume_utc(),
                actor: UserShort {
                    username: pkg.last_updated_user,
                    role: pkg.last_updated_user_role,
                },
            },
            versions: None,
        },
    };

    Ok(response)
}

pub async fn list(lang: Languages, state: web::Data<State>) -> Result<HttpResponse> {
    let records = sqlx::query!(
        r#"
        SELECT name, license, latest_version,
            created.username AS created_user, created.role AS created_user_role,
            last_updated.username AS last_updated_user, last_updated.role AS last_updated_user_role,
            created_date, last_updated_date, star_query.stars
        FROM packages
            LEFT JOIN users AS created
            ON (created.username = created_user)
            LEFT JOIN users AS last_updated
            ON (last_updated.username = last_updated_user)
            LEFT JOIN (
                SELECT package, count(*) as stars FROM stargazers GROUP BY package
            ) AS star_query ON (name = package)
        ORDER BY last_updated_date
        "#
    )
    .fetch_all(&mut &state.db)
    .await?;

    let package_names: Vec<_> = records.iter().map(|rec| rec.name.clone()).collect();

    let summaries = sqlx::query!(
        r#"
        SELECT package, lang, summary as "text"
        FROM summaries
            JOIN packages
            ON (package = packages.name)
        WHERE package = any($1)
        ORDER BY last_updated_date
        "#,
        &package_names
    )
    .fetch_all(&mut &state.db)
    .await?;

    let descriptions = sqlx::query!(
        r#"
        SELECT package, lang, description as "text"
        FROM descriptions
            JOIN packages
            ON (package = packages.name)
        WHERE package = any($1)
        ORDER BY last_updated_date
        "#,
        &package_names
    )
    .fetch_all(&mut &state.db)
    .await?;

    let authors = sqlx::query!(
        r#"
        SELECT package, author
        FROM authors
            JOIN packages
            ON (package = packages.name)
        WHERE package = any($1)
        ORDER BY last_updated_date, author
        "#,
        &package_names
    )
    .fetch_all(&mut &state.db)
    .await?;

    let maintainers = sqlx::query!(
        r#"
        SELECT package, username, role
        FROM maintainers
            INNER JOIN users
            USING (username)
            JOIN packages
            ON (package = packages.name)
        WHERE package = any($1)
        ORDER BY last_updated_date, username
        "#,
        &package_names
    )
    .fetch_all(&mut &state.db)
    .await?;

    let tags = sqlx::query!(
        r#"
        SELECT package, tag, lang, tag_names.name as "text"
        FROM package_tags
            INNER JOIN tag_names
            USING (tag)
            JOIN packages
            ON (package = packages.name)
        WHERE package = any($1)
        ORDER BY last_updated_date, tag
        "#,
        &package_names
    )
    .fetch_all(&mut &state.db)
    .await?;

    let summaries_group = summaries.into_iter().group_by(|rec| rec.package.clone());
    let descriptions_group = descriptions.into_iter().group_by(|rec| rec.package.clone());
    let authors_group = authors.into_iter().group_by(|rec| rec.package.clone());
    let maintainers_group = maintainers.into_iter().group_by(|rec| rec.package.clone());
    let tags_group = tags.into_iter().group_by(|rec| rec.package.clone());

    let iter = zip_by(
        records.into_iter(),
        zip_by_key!(
            zip_by_key!(
                summaries_group.into_iter(),
                descriptions_group.into_iter(),
                0
            ),
            zip_by_key!(
                authors_group.into_iter(),
                zip_by_key!(maintainers_group.into_iter(), tags_group.into_iter(), 0),
                0
            ),
            0
        ),
        |left, right| left.name.cmp(&right.0),
    )
    .filter_map(|arg| match arg {
        (pkg, Some((_name, v))) => Some((pkg, v)),
        _ => None,
    })
    .map(|(pkg, rest)| {
        let (summaries, descriptions) = rest.0.unwrap_or((None, None));
        let (authors, rest) = rest.1.unwrap_or((None, None));
        let (maintainers, tags) = rest.unwrap_or((None, None));

        (pkg, summaries, descriptions, authors, maintainers, tags)
    });

    let mut records = Vec::new();

    for value in iter {
        records.push(match value {
            (Some(pkg), Some(summaries), Some(descriptions), authors, maintainers, tags) => {
                let name = pkg.name;

                Some(Package {
                    summaries: lang.choose(
                        summaries
                            .map(|rec| {
                                Ok(LocalizedText {
                                    lang: Language::try_from(rec.lang).map_err(|e| {
                                        ApiError::ModelError(
                                            e.field(field!(name.clone(), "summaries", "lang"))
                                                .into(),
                                        )
                                    })?,
                                    text: rec.text,
                                })
                            })
                            .collect::<Result<Vec<_>, ApiError>>()?
                            .into_iter(),
                    ),
                    descriptions: lang.choose(
                        descriptions
                            .map(|rec| {
                                Ok(LocalizedText {
                                    lang: Language::try_from(rec.lang).map_err(|e| {
                                        ApiError::ModelError(
                                            e.field(field!(name.clone(), "descriptions", "lang"))
                                                .into(),
                                        )
                                    })?,
                                    text: rec.text,
                                })
                            })
                            .collect::<Result<Vec<_>, ApiError>>()?
                            .into_iter(),
                    ),
                    license: pkg.license,
                    authors: authors
                        .map(Iterator::collect)
                        .unwrap_or_else(Vec::new)
                        .into_iter()
                        .map(|rec| rec.author)
                        .collect(),
                    maintainers: maintainers
                        .map(Iterator::collect)
                        .unwrap_or_else(Vec::new)
                        .into_iter()
                        .map(|rec| UserShort {
                            username: rec.username,
                            role: rec.role,
                        })
                        .collect(),
                    tags: tags
                        .map(Iterator::collect)
                        .unwrap_or_else(Vec::new)
                        .into_iter()
                        .map(|rec| {
                            let tag = rec.tag;
                            let lang = rec.lang;
                            let text = rec.text;

                            Ok((
                                tag,
                                LocalizedText {
                                    lang: Language::try_from(lang).map_err(|e| {
                                        ApiError::ModelError(
                                            e.field(field!(
                                                name.clone(),
                                                "tags",
                                                tag.to_string(),
                                                "lang"
                                            ))
                                            .into(),
                                        )
                                    })?,
                                    text,
                                },
                            ))
                        })
                        .collect::<Result<Vec<_>, ApiError>>()?
                        .into_iter()
                        .group_by(|v| v.0)
                        .into_iter()
                        .map(|(tag, texts)| Tag {
                            id: tag,
                            names: lang.choose(texts.map(|(_, text)| text)),
                        })
                        .collect(),
                    starred: pkg.stars,
                    history: History {
                        latest_version: pkg.latest_version,
                        created: HistoryAction {
                            date: pkg.created_date.assume_utc(),
                            actor: UserShort {
                                username: pkg.created_user,
                                role: pkg.created_user_role,
                            },
                        },
                        last_updated: HistoryAction {
                            date: pkg.last_updated_date.assume_utc(),
                            actor: UserShort {
                                username: pkg.last_updated_user,
                                role: pkg.last_updated_user_role,
                            },
                        },
                        versions: None,
                    },
                    name,
                })
            }
            (pkg, summaries, descriptions, authors, maintainers, tags) => {
                return Err(ApiError::ModelError(anyhow!(
                    "bad package model: {:?}",
                    (
                        pkg,
                        map_to_vec(summaries),
                        map_to_vec(descriptions),
                        map_to_vec(authors),
                        map_to_vec(maintainers),
                        map_to_vec(tags),
                    )
                )))?;
            }
        });
    }

    Ok(HttpResponse::Ok().json(ApiResponse::from(records)))
}

pub async fn get(
    path: web::Path<(String,)>,
    state: web::Data<State>,
    lang: Languages,
) -> Result<HttpResponse> {
    let mut pkg = get_package_info(&path.0, &state.db, &lang).await?;

    // a closure below is eager to move in the whole pkg, despite that &pkg.name suffices
    // so we move the name out here, and move it back into the struct in the end
    let name = pkg.name;

    let versions = sqlx::query!(
        "SELECT version, download_count FROM versions WHERE package = $1",
        name
    )
    .fetch_all(&mut &state.db)
    .await?;

    let version_names = versions
        .iter()
        .map(|v| v.version.clone())
        .collect::<Vec<_>>();

    let dependencies = sqlx::query!(
        r#"
        SELECT version, dependency, spec
        FROM dependencies
        WHERE package = $1 AND version = any($2)
        ORDER BY version
        "#,
        &name,
        &version_names
    )
    .fetch_all(&mut &state.db)
    .await?;

    let requirements = sqlx::query!(
        r#"
        SELECT version, spec, lang, description as "text"
        FROM version_requirements
            LEFT JOIN requirement_descriptions
            USING (spec)
        WHERE package = $1 AND version = any($2)
        ORDER BY version, spec
        "#,
        &name,
        &version_names
    )
    .fetch_all(&mut &state.db)
    .await?;

    let changelogs = sqlx::query!(
        r#"
        SELECT version, lang, changelog AS "text"
        FROM changelogs
        WHERE package = $1 AND version = any($2)
        ORDER BY version
        "#,
        &name,
        &version_names
    )
    .fetch_all(&mut &state.db)
    .await?;

    let dep_group = dependencies.into_iter().group_by(|dep| dep.version.clone());
    let req_group = requirements.into_iter().group_by(|req| req.version.clone());
    let req_descriptions_group = req_group
        .into_iter()
        .map(|(version, requirements)| (version, requirements.group_by(|req| req.spec.clone())));
    let changelog_group = changelogs
        .into_iter()
        .group_by(|changelog| changelog.version.clone());

    struct VersionInfo {
        dependencies: Option<Vec<Dependency>>,
        requirements: Option<Vec<Requirement>>,
        changelogs: Option<Vec<LocalizedText>>,
    }

    let mut version_map: HashMap<String, VersionInfo> = version_names
        .into_iter()
        .map(|v| {
            (
                v,
                VersionInfo {
                    dependencies: None,
                    requirements: None,
                    changelogs: None,
                },
            )
        })
        .collect();

    for (version, dependencies) in &dep_group {
        version_map.get_mut(version.as_str()).unwrap().dependencies = Some(
            dependencies
                .map(|dep| Dependency {
                    package: dep.dependency,
                    spec: dep.spec,
                })
                .collect(),
        );
    }

    for (version, requirements) in req_descriptions_group {
        version_map.get_mut(version.as_str()).unwrap().requirements = Some(
            requirements
                .into_iter()
                .map(|(spec, descriptions)| {
                    Ok(Requirement {
                        spec,
                        descriptions: lang.choose(
                            descriptions
                                .map(|req| {
                                    Ok(LocalizedText {
                                        lang: Language::try_from(req.lang).map_err(|e| {
                                            ApiError::ModelError(
                                                e.field(field!(
                                                    name.clone(),
                                                    "history",
                                                    "versions",
                                                    version.clone(),
                                                    "requirements",
                                                    "lang"
                                                ))
                                                .into(),
                                            )
                                        })?,
                                        text: req.text,
                                    })
                                })
                                .collect::<Result<Vec<_>, ApiError>>()?
                                .into_iter(),
                        ),
                    })
                })
                .collect::<Result<Vec<_>, ApiError>>()?,
        )
    }

    for (version, changelogs) in &changelog_group {
        version_map.get_mut(version.as_str()).unwrap().changelogs = Some(
            changelogs
                .map(|changelog| {
                    Ok(LocalizedText {
                        lang: Language::try_from(changelog.lang).map_err(|e| {
                            ApiError::ModelError(
                                e.field(field!(
                                    name.clone(),
                                    "history",
                                    "versions",
                                    version.clone(),
                                    "changelogs",
                                    "lang"
                                ))
                                .into(),
                            )
                        })?,
                        text: changelog.text,
                    })
                })
                .collect::<Result<Vec<_>, ApiError>>()?,
        );
    }

    let version_info = versions
        .into_iter()
        .map(|version| {
            let VersionInfo {
                dependencies,
                requirements,
                changelogs,
            } = version_map.remove(&version.version).unwrap_or(VersionInfo {
                dependencies: None,
                requirements: None,
                changelogs: None,
            });

            let changelogs = if let Some(v) = changelogs {
                lang.choose(v.into_iter())
            } else {
                vec![]
            };

            Version {
                version: version.version,
                downloads: version.download_count,
                archive_url: "TODO".to_owned(),
                file_index_url: "TODO".to_owned(),
                requirements: requirements.unwrap_or_else(Vec::new),
                dependencies: dependencies.unwrap_or_else(Vec::new),
                changelogs,
            }
        })
        .collect::<Vec<_>>();

    pkg.name = name;
    pkg.history.versions = Some(version_info);

    Ok(HttpResponse::Ok().json(ApiResponse::from(pkg)))
}

async fn new_package(
    path: web::Path<(String,)>,
    data: web::Json<NewPackage>,
    state: web::Data<State>,
) -> Result<HttpResponse> {
    type Vec2<A, B> = (Vec<A>, Vec<B>);

    let entry = sqlx::query!("SELECT name FROM packages WHERE name = $1", path.0)
        .fetch_optional(&mut &state.db)
        .await?;

    if entry.is_some() {
        Err(ApiError::PackageExists(path.0.clone()))?;
    }

    let data = data.into_inner();

    if let Err(err) = data.validate(&state).await {
        if let ValidationErrorKind::SqlError(sql_error) = err.kind {
            Err(sql_error)?;
        } else {
            Err(err)?;
        }
    }

    let mut tx = state.db.begin().await?;
    let now = OffsetDateTime::now();
    let now = time::PrimitiveDateTime::new(now.date(), now.time());

    sqlx::query!(
        r#"
        INSERT INTO packages (
            name,
            license,
            latest_version,
            created_date,
            created_user,
            last_updated_date,
            last_updated_user
        ) VALUES ($1, $2, NULL, $3, $4, $5, $6)
        "#,
        path.0,
        data.license,
        now,
        "TODO".to_owned(),
        now,
        "TODO".to_owned()
    )
    .execute(&mut tx)
    .await?;

    let (langs, summaries): Vec2<_, _> = data
        .summaries
        .into_iter()
        .map(|LocalizedText { lang, text }| (lang.to_string(), text))
        .unzip();

    sqlx::query!(
        r#"
        INSERT INTO summaries (
            package,
            lang,
            summary
        ) SELECT $1 AS package, lang, summary
            FROM unnest($2::text[], $3::text[])
                AS query(lang, summary)
        "#,
        path.0,
        &langs,
        &summaries
    )
    .execute(&mut tx)
    .await?;

    let (langs, descriptions): Vec2<_, _> = data
        .descriptions
        .into_iter()
        .map(|LocalizedText { lang, text }| (lang.to_string(), text))
        .unzip();

    sqlx::query!(
        r#"
        INSERT INTO descriptions (
            package,
            lang,
            description
        ) SELECT $1 AS package, lang, description
            FROM unnest($2::text[], $3::text[])
                AS query(lang, description)
        "#,
        path.0,
        &langs,
        &descriptions
    )
    .execute(&mut tx)
    .await?;

    sqlx::query!(
        r#"
        INSERT INTO authors (
            package,
            author
        ) SELECT $1 AS package, author
            FROM unnest($2::text[])
                AS query(author)
        "#,
        path.0,
        &data.authors
    )
    .execute(&mut tx)
    .await?;

    sqlx::query!(
        r#"
        INSERT INTO maintainers (
            package,
            username
        ) SELECT $1 AS package, username
            FROM unnest($2::text[])
                AS query(username)
        "#,
        path.0,
        &data.maintainers
    )
    .execute(&mut tx)
    .await?;

    sqlx::query!(
        r#"
        INSERT INTO package_tags (
            package,
            tag
        ) SELECT $1 AS package, tag
            FROM unnest($2::integer[])
                AS query(tag)
        "#,
        path.0,
        &data.tags
    )
    .execute(&mut tx)
    .await?;

    let versions = data
        .versions
        .iter()
        .map(|Version { version, .. }| version.clone())
        .collect::<Vec<_>>();

    sqlx::query!(
        r#"
        INSERT INTO versions (
            package,
            version,
            download_count
        ) SELECT $1 AS package, version, 0
            FROM unnest($2::text[])
                AS query(version)
        "#,
        path.0,
        &versions
    )
    .execute(&mut tx)
    .await?;

    let (dependencies, tail): Vec2<_, _> = data
        .versions
        .into_iter()
        .flat_map(
            |Version {
                 version,
                 dependencies,
                 requirements,
                 changelogs,
                 ..
             }| {
                let version1 = version.clone();
                let version2 = version.clone();

                dependencies
                    .into_iter()
                    .map(move |Dependency { package, spec }| (version.clone(), (package, spec)))
                    .zip(
                        requirements
                            .into_iter()
                            .map(move |Requirement { spec, .. }| (version1.clone(), spec))
                            .zip(changelogs.into_iter().map(
                                move |LocalizedText { lang, text }| {
                                    (version2.clone(), (lang.to_string(), text))
                                },
                            )),
                    )
            },
        )
        .unzip();

    let (requirements, changelogs): Vec2<_, _> = tail.into_iter().unzip();

    {
        let (versions, packages_specs): Vec2<_, _> = dependencies.into_iter().unzip();
        let (packages, specs): Vec2<_, _> = packages_specs.into_iter().unzip();

        sqlx::query!(
            r#"
            INSERT INTO dependencies (
                package,
                version,
                dependency,
                spec
            ) SELECT $1 AS package, version, dependency, spec
                FROM unnest($2::text[], $3::text[], $4::text[])
                    AS query(version, dependency, spec)
            "#,
            path.0,
            &versions,
            &packages,
            &specs
        )
        .execute(&mut tx)
        .await?;
    }

    {
        let (versions, specs): Vec2<_, _> = requirements.into_iter().unzip();

        sqlx::query!(
            r#"
            INSERT INTO version_requirements (
                package,
                version,
                spec
            ) SELECT $1 AS package, version, spec
                FROM unnest($2::text[], $3::text[])
                    AS query(version, spec)
            "#,
            path.0,
            &versions,
            &specs
        )
        .execute(&mut tx)
        .await?;
    }

    {
        let (versions, langs_texts): Vec2<_, _> = changelogs.into_iter().unzip();
        let (langs, texts): Vec2<_, _> = langs_texts.into_iter().unzip();

        sqlx::query!(
            r#"
            INSERT INTO changelogs (
                package,
                version,
                lang,
                changelog
            ) SELECT $1 AS package, version, lang, changelog
                FROM unnest($2::text[], $3::text[], $4::text[])
                    AS query(version, lang, changelog)
            "#,
            path.0,
            &versions,
            &langs,
            &texts
        )
        .execute(&mut tx)
        .await?;
    }

    tx.commit().await?;

    Ok(HttpResponse::Created().json(ApiResponse::<()>::from(None)))
}
