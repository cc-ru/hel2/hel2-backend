use actix_session::Session;
use actix_web::{web, HttpResponse};

use crate::error::{ApiError, SqlError};
use crate::State;

use super::models::{ApiResponse, LoginCredentials, SessionData};
use super::Result;

pub fn configure(cfg: &mut web::ServiceConfig) {
    cfg.route("/login", web::post().to(login));
}

pub async fn login(
    data: web::Json<LoginCredentials>,
    session: Session,
    state: web::Data<State>,
) -> Result {
    let record = sqlx::query!(
        "SELECT username, password_hash, salt, role FROM users WHERE username = $1",
        &data.username
    )
    .fetch_one(&state.db)
    .await
    .map_err(|e| match e {
        SqlError::RowNotFound => ApiError::InvalidCredentials,
        _ => ApiError::from(e),
    })?;

    match argon2::verify_raw(
        data.password.as_bytes(),
        &record.salt,
        &record.password_hash,
        &state.argon2,
    ) {
        Ok(true) => {
            let data = SessionData {
                username: record.username,
                role: record.role,
            };

            session.set("token", &data).map_err(|e| {
                ApiError::InternalError(Some(anyhow::anyhow!(
                    "Could not set a session cookie for user {}: {:?}",
                    data.username,
                    e
                )))
            })?;

            Ok(HttpResponse::Ok().json(ApiResponse::<()>::from(None)))
        }
        _ => Err(ApiError::InvalidCredentials),
    }
}
