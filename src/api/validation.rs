use std::fmt::{self, Display, Formatter};
use std::ops::{Bound, RangeBounds};

use crate::error::SqlError;
use crate::State;

#[derive(Debug)]
pub struct Field(Vec<String>);

impl Field {
    pub fn new() -> Self {
        Self(Vec::new())
    }

    pub fn prepend(mut self, other: impl Into<Field>) -> Self {
        let mut other = other.into();

        for item in other.0.drain(..) {
            self.0.push(item);
        }

        self
    }

    pub fn append(mut self, other: impl Into<Field>) -> Self {
        let mut other = other.into();

        for item in self.0.drain(..) {
            other.0.push(item);
        }

        self.0 = other.0;

        self
    }
}

impl<T: Into<String>> From<T> for Field {
    fn from(s: T) -> Self {
        Self(vec![s.into()])
    }
}

impl Display for Field {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for (i, field) in self.0.iter().rev().enumerate() {
            let index = match field.find(|c: char| !c.is_ascii_alphanumeric() && c != '_') {
                Some(_) => false,
                None => true,
            };

            if i != 0 {
                write!(f, "{}", if index { "[" } else { "." })?;
            }

            if index {
                write!(f, "{}", field)?;
            } else {
                write!(f, "\"{}\"", field)?;
            }

            if i != 0 && index {
                write!(f, "]")?;
            }
        }

        Ok(())
    }
}

#[derive(Debug)]
pub struct ValidationError {
    pub field: Field,
    pub kind: ValidationErrorKind,
}

impl Display for ValidationError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "Validation error in field {}: {}", self.field, self.kind)
    }
}

impl std::error::Error for ValidationError {}

#[derive(Debug)]
pub enum ValidationErrorKind {
    NoDataSpecified,
    RangeNotSatisfied(Bound<usize>, Bound<usize>),
    DuplicateValue(String),
    InvalidLanguage(String),
    PackageNotFound(String),
    UserNotFound(String),
    TagNotFound(i32),
    InvalidVersion(String),
    InvalidVersionSpec(String),
    InvalidRequirement(String),

    SqlError(SqlError),
}

use ValidationErrorKind::*;

impl Display for ValidationErrorKind {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            NoDataSpecified => write!(f, "no data specified"),
            RangeNotSatisfied(start, end) => {
                use Bound::*;

                let left = match start {
                    Included(n) => format!("[{}", n),
                    Excluded(n) => format!("({}", n),
                    Unbounded => "(-∞".to_owned(),
                };

                let right = match end {
                    Included(n) => format!("{}]", n),
                    Excluded(n) => format!("{})", n),
                    Unbounded => "+∞)".to_owned(),
                };

                write!(f, "range not satisfied: {}; {}", left, right)
            }
            DuplicateValue(value) => write!(f, "duplicate value: {}", value),
            InvalidLanguage(not_lang) => write!(f, "invalid language: {}", not_lang),
            PackageNotFound(name) => write!(f, "package not found: {}", name),
            UserNotFound(username) => write!(f, "user not found: {}", username),
            TagNotFound(tag) => write!(f, "tag not found: {}", tag),
            InvalidVersion(not_version) => write!(f, "invalid version: {}", not_version),
            InvalidVersionSpec(spec) => {
                write!(f, "invalid version requirement specification: {}", spec)
            }
            InvalidRequirement(spec) => write!(f, "invalid requirement: {}", spec),

            SqlError(_) => write!(f, "internal error"),
        }
    }
}

impl From<SqlError> for ValidationErrorKind {
    fn from(error: SqlError) -> Self {
        Self::SqlError(error)
    }
}

pub type ValidationResult<T> = Result<T, ValidationError>;

pub trait ValidationResultExt {
    type Output;

    fn field(self, field: impl Into<Field>) -> Self::Output;
    fn subfield(self, subfield: impl Into<Field>) -> Self::Output;
}

impl<T> ValidationResultExt for ValidationResult<T> {
    type Output = ValidationResult<T>;

    fn field(self, field: impl Into<Field>) -> Self::Output {
        self.map_err(|mut e| {
            e.field = e.field.prepend(field);
            e
        })
    }

    fn subfield(self, subfield: impl Into<Field>) -> Self::Output {
        self.map_err(|mut e| {
            e.field = e.field.append(subfield);
            e
        })
    }
}

impl<T> ValidationResultExt for Result<T, ValidationErrorKind> {
    type Output = ValidationResult<T>;

    fn field(self, field: impl Into<Field>) -> Self::Output {
        self.map_err(move |e| ValidationError {
            field: field.into(),
            kind: e,
        })
    }

    fn subfield(self, field: impl Into<Field>) -> Self::Output {
        self.map_err(move |e| ValidationError {
            field: field.into(),
            kind: e,
        })
    }
}

impl ValidationResultExt for ValidationErrorKind {
    type Output = ValidationError;

    fn field(self, field: impl Into<Field>) -> Self::Output {
        ValidationError {
            field: field.into(),
            kind: self,
        }
    }

    fn subfield(self, subfield: impl Into<Field>) -> Self::Output {
        ValidationError {
            field: subfield.into(),
            kind: self,
        }
    }
}

impl ValidationResultExt for ValidationError {
    type Output = ValidationError;

    fn field(mut self, field: impl Into<Field>) -> Self::Output {
        self.field = self.field.prepend(field);
        self
    }

    fn subfield(mut self, subfield: impl Into<Field>) -> Self::Output {
        self.field = self.field.append(subfield);
        self
    }
}

#[async_trait]
pub trait Validate {
    type Output;

    async fn validate(&self, state: &State) -> ValidationResult<Self::Output>;
}

pub fn validate_length(s: &str, range: impl RangeBounds<usize>) -> Result<(), ValidationErrorKind> {
    if range.contains(&s.len()) {
        Ok(())
    } else {
        fn clone_bound(bound: Bound<&usize>) -> Bound<usize> {
            match bound {
                Bound::Included(&v) => Bound::Included(v.clone()),
                Bound::Excluded(&v) => Bound::Excluded(v.clone()),
                Bound::Unbounded => Bound::Unbounded,
            }
        }

        Err(RangeNotSatisfied(
            clone_bound(range.start_bound()),
            clone_bound(range.end_bound()),
        ))
    }
}

pub async fn validate_users(users: &[String], mut pool: &sqlx::PgPool) -> ValidationResult<()> {
    match sqlx::query!(
        r#"
        SELECT username
        FROM unnest($1::text[])
            AS query(username)
        WHERE username NOT IN (
            SELECT username FROM users
        )
        LIMIT 1
        "#,
        users
    )
    .fetch_optional(&mut pool)
    .await
    {
        Ok(Some(user)) => {
            let username = user.username;
            let i = users
                .iter()
                .enumerate()
                .find(|(_, x)| **x == username)
                .map(|(i, _)| i)
                .unwrap();

            Err(ValidationErrorKind::UserNotFound(username).field(i.to_string()))
        }
        Ok(None) => Ok(()),
        Err(e) => Err(ValidationErrorKind::SqlError(e).field(field!())),
    }
}

pub async fn validate_tags(tags: &[i32], mut pool: &sqlx::PgPool) -> ValidationResult<()> {
    match sqlx::query!(
        r#"
        SELECT tag
        FROM unnest($1::integer[])
            AS query(tag)
        WHERE tag NOT IN (
            SELECT id FROM tags
        )
        LIMIT 1
        "#,
        tags
    )
    .fetch_optional(&mut pool)
    .await
    {
        Ok(Some(user)) => {
            let tag = user.tag;
            let i = tags
                .iter()
                .enumerate()
                .find(|(_, x)| **x == tag)
                .map(|(i, _)| i)
                .unwrap();

            Err(ValidationErrorKind::TagNotFound(tag).field(i.to_string()))
        }
        Ok(None) => Ok(()),
        Err(e) => Err(ValidationErrorKind::SqlError(e).field(field!())),
    }
}

pub fn validate_version(version: &str) -> Result<(), ValidationErrorKind> {
    match semver::Version::parse(version) {
        Ok(_) => Ok(()),
        Err(_) => Err(ValidationErrorKind::InvalidVersion(version.into())),
    }
}

pub fn validate_version_spec(spec: &str) -> Result<(), ValidationErrorKind> {
    match semver::VersionReq::parse(spec) {
        Ok(_) => Ok(()),
        Err(_) => Err(ValidationErrorKind::InvalidVersionSpec(spec.into())),
    }
}

pub async fn validate_dependencies(
    dependencies: Vec<(usize, usize, &String)>,
    mut pool: &sqlx::PgPool,
) -> Result<(), (Option<(usize, usize)>, ValidationErrorKind)> {
    let param = dependencies
        .iter()
        .map(|(_, _, dep)| (*dep).clone())
        .collect::<Vec<String>>();

    match sqlx::query!(
        r#"
        SELECT name
        FROM unnest($1::text[])
            AS query(name)
        WHERE name NOT IN (
            SELECT name FROM packages
        )
        LIMIT 1
        "#,
        &param
    )
    .fetch_optional(&mut pool)
    .await
    {
        Ok(Some(dep)) => {
            let name = dep.name;
            let (i, j) = dependencies
                .iter()
                .find(|(_, _, dep)| **dep == name)
                .map(|(i, j, _)| (*i, *j))
                .unwrap();

            Err((Some((i, j)), ValidationErrorKind::PackageNotFound(name)))
        }
        Ok(None) => Ok(()),
        Err(e) => Err((None, ValidationErrorKind::SqlError(e))),
    }
}

pub async fn validate_requirements(
    requirements: Vec<(usize, usize, &String)>,
    mut pool: &sqlx::PgPool,
) -> Result<(), (Option<(usize, usize)>, ValidationErrorKind)> {
    let param = requirements
        .iter()
        .map(|(_, _, req)| (*req).clone())
        .collect::<Vec<String>>();

    match sqlx::query!(
        r#"
        SELECT spec
        FROM unnest($1::text[])
            AS query(spec)
        WHERE spec NOT IN (
            SELECT spec FROM requirements
        )
        LIMIT 1
        "#,
        &param
    )
    .fetch_optional(&mut pool)
    .await
    {
        Ok(Some(req)) => {
            let spec = req.spec;
            let (i, j) = requirements
                .iter()
                .find(|(_, _, req)| **req == spec)
                .map(|(i, j, _)| (*i, *j))
                .unwrap();

            Err((Some((i, j)), ValidationErrorKind::InvalidRequirement(spec)))
        }
        Ok(None) => Ok(()),
        Err(e) => Err((None, ValidationErrorKind::SqlError(e))),
    }
}
