#[macro_use]
extern crate async_trait;

pub mod api;
pub mod config;
pub mod error;

use actix_session::CookieSession;
use actix_web::{web, App, HttpServer};
use anyhow::Result;
use sqlx::PgPool;

#[derive(Clone)]
pub struct State {
    pub db: PgPool,
    pub argon2: argon2::Config<'static>,
}

#[actix_rt::main]
async fn main() -> Result<()> {
    let cfg = config::load(&std::env::var("HEL2_CONFIG").unwrap_or("config.toml".to_owned()))?;

    let db_pool = PgPool::new(&cfg.db_url).await?;
    let state = web::Data::new(State {
        db: db_pool,
        argon2: argon2::Config {
            ad: &[],
            hash_length: 32,
            lanes: 1,
            mem_cost: 4096,
            secret: &[],
            thread_mode: argon2::ThreadMode::Sequential,
            time_cost: 3,
            variant: argon2::Variant::Argon2i,
            version: argon2::Version::Version13,
        },
    });

    let session_type = cfg.session.r#type;
    let session_key = cfg.session.key;

    Ok(HttpServer::new(move || {
        let cookie_session = match session_type {
            config::SessionType::Signed => CookieSession::signed(&session_key),
            config::SessionType::Private => CookieSession::private(&session_key),
        };

        App::new()
            .app_data(state.clone())
            .wrap(api::ResponseWrapper)
            .wrap(cookie_session)
            .service(web::scope("/packages").configure(api::packages::configure))
            .service(web::scope("/auth").configure(api::auth::configure))
    })
    .bind(cfg.address)?
    .run()
    .await?)
}
