use std::path::Path;

use serde::Deserialize;
use serde_bytes::ByteBuf;
use time::Duration;

#[derive(Deserialize, Clone)]
#[serde(rename_all = "lowercase")]
pub enum SessionType {
    Signed,
    Private,
}

#[derive(Deserialize)]
pub struct Session {
    pub r#type: SessionType,
    pub key: ByteBuf,
    pub name: String,
    pub domain: Option<String>,
    pub path: Option<String>,
    pub secure: Option<bool>,
    pub http_only: Option<bool>,
    pub max_age: Duration,
}

#[derive(Deserialize)]
pub struct Config {
    pub db_url: String,
    pub address: String,
    pub session: Session,
}

pub fn load(path: impl AsRef<Path>) -> std::io::Result<Config> {
    let contents = std::fs::read_to_string(path)?;

    Ok(toml::from_str(&contents)?)
}
